class OhGodItsTheEndOfTheWorld < StandardError; end;

class Rover
  attr_reader :pos_x, :pos_y
  attr_reader :heading # in degrees
  
  ROTATION_DEGREES    = 360
  ROTATION_ANGLE      = 90
  ROTATION_HEADINGS   = {'N'=>0,'E'=>90,'S'=>180,'W'=>270}.freeze
  ROTATION_DIRECTIONS = ['N','E','S','W'].freeze
  
  class << self
    attr_accessor :world_x, :world_y
    
    def heading_for(direction)
      return ROTATION_HEADINGS[direction]
    end
    
    def direction_for(heading)
      if (heading >= ROTATION_DEGREES)
        heading = heading % ROTATION_DEGREES
      end
      heading = 0 if heading == ROTATION_DEGREES # hack?
      return ROTATION_DIRECTIONS[heading / ROTATION_ANGLE]
    end
  end
  
  def initialize(pos_x = 0, pos_y = 0, heading = 0)
    @pos_x, @pos_y, @heading = pos_x, pos_y, heading
  end
  
  def turn_cw
    @heading = heading + ROTATION_ANGLE
  end
  
  def turn_ccw
    @heading = heading - ROTATION_ANGLE
  end
  
  def advance
    case Rover.direction_for(heading)
    when 'N'
      raise OhGodItsTheEndOfTheWorld if @pos_y + 1 > Rover.world_y
      @pos_y = @pos_y + 1
    when 'E'
      raise OhGodItsTheEndOfTheWorld if @pos_x + 1 > Rover.world_x
      @pos_x = @pos_x + 1
    when 'S'
      raise OhGodItsTheEndOfTheWorld if @pos_y - 1 < 0
      @pos_y = @pos_y - 1
    when 'W'
      raise OhGodItsTheEndOfTheWorld if @pos_x - 1 < 0
      @pos_x = @pos_x - 1
    end
  end
  
  def execute(command)
    command.each_char do |char|
      case char
      when 'M'
        advance
      when 'L'
        turn_ccw
      when 'R'
        turn_cw
      else
        raise ArgumentError
      end
    end
  end
  
  def report
    return [pos_x, pos_y, Rover.direction_for(heading)]
  end
  
  def directed?(where)
    return Rover.direction_for(heading) == where
  end
end