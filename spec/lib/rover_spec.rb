require 'spec_helper'

describe Rover do
  before do
    Rover.world_x = 5
    Rover.world_y = 5
  end
  
  describe "heading and direction utility methods" do
    it "should return a direction for a given heading" do
      Rover.direction_for(0).should == 'N'
      Rover.direction_for(90).should == 'E'
      Rover.direction_for(180).should == 'S'
      Rover.direction_for(270).should == 'W'
    end
    
    it "should return a heading for a given direction" do
      Rover.heading_for('N').should == 0
      Rover.heading_for('E').should == 90
      Rover.heading_for('S').should == 180
      Rover.heading_for('W').should == 270
    end
  end
  
  describe "defaults" do
    before do
      @rover = Rover.new
    end
    
    it "should be at (0,0)" do
      @rover.pos_x.should == 0
      @rover.pos_y.should == 0
    end
  
    it "should be directed north" do
      @rover.should be_directed('N')
    end
  end
  
  describe "rotation" do
    before do
      @rover = Rover.new
    end
    
    it "should rotate clockwise" do
      @rover.turn_cw
      @rover.should be_directed('E')
    end
    
    it "should rotate counter-clockwise" do
      @rover.turn_ccw
      @rover.should be_directed('W')
    end
  end
  
  describe "advancing" do
    it "should move 1 unit positive on the y axis when facing north" do
      @rover = Rover.new(1,1,0)
      @rover.advance
      @rover.pos_y.should == 2
    end
    
    it "should move 1 unit positive on the x axis when facing east" do
      @rover = Rover.new(1,1,90)
      @rover.advance
      @rover.pos_x.should == 2
    end
    
    it "should move 1 unit negative on the x axis when facing south" do
      @rover = Rover.new(1,1,180)
      @rover.advance
      @rover.pos_y.should == 0
    end
    
    it "should move 1 unit negative on the x axis when facing west" do
      @rover = Rover.new(1,1,270)
      @rover.advance
      @rover.pos_x.should == 0
    end
    
    describe "at the end of the world" do
      def advance
        lambda {
          @rover.advance
        }.should raise_error(OhGodItsTheEndOfTheWorld)
      end
    
      it "shouldn't move north" do
        @rover = Rover.new(1,Rover.world_y,0)
        advance
      end
    
      it "shouldn't move east" do
        @rover = Rover.new(Rover.world_x,1,90)
        advance
      end
    
      it "shouldn't move south" do
        @rover = Rover.new(1,0,180)
        advance
      end
    
      it "shouldn't move west" do
        @rover = Rover.new(0,1,270)
        advance
      end
    end
  end
  
  describe "executing commands" do
    before do
      @rover = Rover.new
    end
    
    it "should advance when it encounters an 'M'" do
      @rover.should_receive(:advance).exactly(2).times
      @rover.execute "MM"
    end
    
    it "should turn clockwise when it encounters an 'R'" do
      @rover.should_receive(:turn_cw).exactly(2).times
      @rover.execute "RR"
    end
    
    it "should turn counter-clockwise when it encounters an 'L'" do
      @rover.should_receive(:turn_ccw).exactly(2).times
      @rover.execute "LL"
    end
  end
end