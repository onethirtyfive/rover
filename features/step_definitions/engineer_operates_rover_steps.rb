require 'rover'

Given /^the geography of interest is bounded (\d+) units north, (\d+) units east$/ do |world_x, world_y|
  Rover.world_x = world_x.to_i
  Rover.world_y = world_y.to_i
end

Given /^the rover has landed$/ do
  @rover = Rover.new
end

Given /^the rover has landed at \((\d+),(\d+)\) facing (North|East|South|West)$/ do |pos_x, pos_y, direction|
  @rover = Rover.new(pos_x.to_i, pos_y.to_i, Rover.heading_for(direction[0]))
end

When /^I issue the command "([^"]*)"$/ do |command|
  @rover.execute(command)
end

When /^I ask (it|the rover) for a positional report$/ do |ignore|
  @position = @rover.report
end

Then /^the result should be \((\d+),(\d+),(N|S|W|E)\)$/ do |x, y, dir|
  @position.should == [x.to_i, y.to_i, dir]
end

When /^I turn it 90 degrees (clockwise|counter-clockwise)$/ do |bearing|
  turn_method = 'turn_' + (bearing == 'clockwise' ? 'cw' : 'ccw' )
  @rover.send turn_method.to_sym
end

Then /^it should be directed (North|East|South|West)$/ do |direction|
  @rover.should be_directed(direction[0])
end

When /^I advance the rover$/ do
  @rover.advance
end
