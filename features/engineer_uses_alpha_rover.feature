Feature: Engineer uses alpha rover
  In order to gather samples
  As a NASA space engineer
  I want to operate Rover Alpha
  
  Scenario: Navigating to Site A (1,3)
    Given the rover has landed at (1,2) facing North
    When I issue the command "LMLMLMLMM"
    And I ask the rover for a positional report
    Then the result should be (1,3,N)