Feature: Engineer uses beta rover
  In order to gather samples
  As a NASA space engineer
  I want to operate Rover Beta
  
  Scenario: Navigating to Site B (5,1)
    Given the rover has landed at (3,3) facing East
    When I issue the command "MMRMMRMRRM"
    And I ask the rover for a positional report
    Then the result should be (5,1,E)