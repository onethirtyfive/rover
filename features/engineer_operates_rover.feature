Feature: Engineer operates rover
  In order to further space exploration
  As a NASA space engineer
  I want to reliably navigate our Mars rover
  
  Background:
    Given the rover has landed
    And the geography of interest is bounded 5 units north, 5 units east
  
  Scenario: Checking in
    When I ask it for a positional report
    Then the result should be (0,0,N)
  
  Scenario: Turning rover 90 degrees clockwise
    When I turn it 90 degrees clockwise
    Then it should be directed East
    
  Scenario: Turning rover 90 degrees counter-clockwise
    When I turn it 90 degrees counter-clockwise
    Then it should be directed West
  
  Scenario: Advancing the rover
    When I advance the rover
    And I ask it for a positional report
    Then the result should be (0,1,N)
